package android.guthrieh.crystalball;

public class Predictions {

    public static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wishes will come true. Just Kidding",
                "Your wishes will NEVER come true",
                "Your a failure",
                "bye"

        };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPredictions() {
        int range = (answers.length - 1);
        int rand = (int)(Math.random() * range) + 1;
        return answers[rand];
    }
}
